#!/bin/sh

pid=/tmp/lounge.pid

case $1 in
  stop) cat $pid | xargs kill ;;
  *)
    chown -R lounge:lounge /var/lib/lounge
    rm -f $pid
    daemonize -p $pid -l $pid -u lounge /usr/local/bin/thelounge start ;;
esac
