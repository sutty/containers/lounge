FROM sutty/daemonize:latest AS build
MAINTAINER "f <f@sutty.nl>"

RUN echo /home/builder/packages/home > /etc/apk/repositories
RUN apk add --no-cache daemonize

FROM sutty/monit:latest

RUN apk add --no-cache yarn
RUN yarn --non-interactive --frozen-lockfile global add thelounge

RUN addgroup -S lounge
RUN adduser -h /var/lib/lounge -s /bin/sh -G lounge -S -u 9000 lounge

COPY ./sutty.patch /tmp/sutty.patch
RUN apk add --no-cache patch \
    && cd /usr/local/share/.config/yarn/global/node_modules/thelounge \
    && patch -Np 0 -i /tmp/sutty.patch \
    && apk del patch

COPY --from=build /usr/sbin/daemonize /usr/sbin/daemonize
COPY ./lounge.sh /usr/local/bin/lounge
RUN chmod ugo+rx /usr/local/bin/lounge
COPY ./monit.conf /etc/monit.d/lounge.conf
RUN monit -t

EXPOSE 9000
VOLUME /var/lib/lounge
